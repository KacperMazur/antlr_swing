package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globals1 = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer plus(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer minus(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer div(Integer a, Integer b) {
		if(b != 0)
		{
			return a /b;
			
		}else
		{
			System.out.println("You can't div by 0!");
			return 0;
		}
	}
	
}
