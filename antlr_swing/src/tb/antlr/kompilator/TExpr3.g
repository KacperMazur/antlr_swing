tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;


zakres : ^(BEGIN  {enterScope();} (e+=zakres | e+=expr | d+=decl)* {leaveScope();}) -> blok (wyr={$e}, dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> minus (p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podst(name = {$i1.text}, value={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(name = {$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {number++;} -> if(cond = {$e1.st}, truee= {$e2.st}, falsee= {$e3.st}, number = {number.toString()})
    ;
    